package org.sambasoft.repositories;

import org.sambasoft.entities.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country,Integer>{

}
