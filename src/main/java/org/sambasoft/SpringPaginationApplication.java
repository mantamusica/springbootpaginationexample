package org.sambasoft;

import org.sambasoft.entities.Country;
import org.sambasoft.repositories.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPaginationApplication implements CommandLineRunner{
	
	@Autowired
	private CountryRepository countryRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringPaginationApplication.class, args);
	}
	
	public void run(String... args) throws Exception{
		countryRepository.save(new Country("china", "pekin"));
		countryRepository.save(new Country("españa", "madrid"));
		countryRepository.save(new Country("eeuu", "new york"));
		countryRepository.save(new Country("francia", "paris"));
		countryRepository.save(new Country("argentina", "buenos aires"));
		countryRepository.save(new Country("portugal", "lisboa"));
		countryRepository.save(new Country("italia", "roma"));
		countryRepository.save(new Country("peru", "lima"));
		countryRepository.save(new Country("inglaterra", "londres"));
		countryRepository.save(new Country("alemania", "berlin"));
		countryRepository.save(new Country("suecia", "oslo"));
		countryRepository.save(new Country("japon", "tokyo"));
		countryRepository.save(new Country("rusia", "moscu"));
		countryRepository.save(new Country("mexico", "mexico dc"));
		countryRepository.save(new Country("brasil", "rio janeiro"));
	}

}
